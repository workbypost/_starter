# Post Starter Kit 

1. Clone [Kirby Starterkit](https://github.com/getkirby/starterkit) into this project folder.

2. Remove git entirely

3. Add custom content repo or use default content

4. Add scss & scripts folder to assets with app.scss|js in each.

5. npm install.

6. panel.install true in /site/config/config.php.

7. gulp watch.

8. Set up new Repo for project.

9. Make website :)
